import * as mongoose from 'mongoose';

export const AnnotatorSchema = new mongoose.Schema({
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    active_sessions: [{
        job_id: String,
        session_progress: Number,
        detections_qty: Number,
        detections_with_track_id: Number,
    }],
});
