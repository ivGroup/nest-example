import { Injectable } from '@nestjs/common';
import { Annotator } from '../interfaces';
import { getAnnotator } from './fixture/annotators.fixture';

@Injectable()
export class AnnotatorsRepository {
    async findOneById(id: string): Promise<Annotator> {
        return getAnnotator();
    }

    // async findAll(): Promise<Annotator[]> {
    //     return [getAnnotator()];
    // }
}
