import { Module } from '@nestjs/common';
import { AnnotatorsController } from './annotators.controller';
import { CommandHandlers } from './commands/handlers';
import { EventHandlers } from './events/handlers';
import { QueryHandlers } from './queries/handlers';
import { AnnotatorsSagas } from './sagas/annotators.sagas';
import { AnnotatorsRepository } from './repository/annotators.repository';
import { MongooseModule } from '@nestjs/mongoose';
import { AnnotatorSchema } from './schemas/annotator.schema';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
    imports: [
        CqrsModule,
        // MongooseModule.forFeature([{ name: 'Annotator', schema: AnnotatorSchema }]),
    ],
    controllers: [AnnotatorsController],
    providers: [
        AnnotatorsRepository,
        ...CommandHandlers,
        ...EventHandlers,
        ...QueryHandlers,
        AnnotatorsSagas,
    ],
})
export class AnnotatorsModule {}
