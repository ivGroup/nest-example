export class FinishSessionCommand {
    constructor(
        public readonly annotatorId: string,
        public readonly jobId: string,
    ) {}
}
