import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../repository/annotators.repository';
import { UpdateSessionProgressCommand } from '../impl/update-session-progress.command';

@CommandHandler(UpdateSessionProgressCommand)
export class UpdateAnnotatorSessionProgressHandler
    implements ICommandHandler<UpdateSessionProgressCommand> {
    constructor(
        private readonly repository: AnnotatorsRepository,
        private readonly publisher: EventPublisher,
    ) {}

    async execute(command: UpdateSessionProgressCommand) {

        // const { heroId, itemId } = command;
        // const hero = this.publisher.mergeObjectContext(
        //     await this.repository.findOneById(+heroId),
        // );
        // hero.addItem(itemId);
        // hero.commit();
    }
}
