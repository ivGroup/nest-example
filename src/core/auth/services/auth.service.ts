import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { QueryBus } from '@nestjs/cqrs';
import { GetAnnotatorQuery } from '../../../modules/annotators/queries/impl';
import { Annotator } from '../../../modules/annotators/interfaces';

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly queryBus: QueryBus,
    ) {}

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.queryBus.execute<GetAnnotatorQuery, Annotator>(new GetAnnotatorQuery(username));

        if (user && user.password === pass) {
            const { password, ...result } = user;
            return result;
        }

        return null;
    }

    async login(user: any) {
        const payload = { username: user.email, sub: user._id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
