import { Exclude } from 'class-transformer';
import { Session } from '../interfaces';

/* tslint:disable:variable-name */
export class AnnotatorEntity {
    _id: string;
    first_name: string;
    last_name: string;
    email: string;
    active_sessions: Session[];

    @Exclude()
    password: string;

    constructor(partial: Partial<AnnotatorEntity>) {
        Object.assign(this, partial);
    }
}
