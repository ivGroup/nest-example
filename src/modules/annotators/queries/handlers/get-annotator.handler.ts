import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AnnotatorsRepository } from '../../repository/annotators.repository';
import { GetAnnotatorQuery } from '../impl';
import { Annotator } from '../../interfaces';

@QueryHandler(GetAnnotatorQuery)
export class GetAnnotatorHandler implements IQueryHandler<GetAnnotatorQuery> {
    constructor(private readonly repository: AnnotatorsRepository) {}

    async execute({ id }: GetAnnotatorQuery): Promise<Annotator> {
        return this.repository.findOneById(id);
    }
}
