import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService } from '../../shared/config/services';
import { ENV_VARIABLE_KEY } from '../../shared/config/constants';

@Module({
    imports: [
        // MongooseModule.forRootAsync({
        //     inject: [ConfigService],
        //     useFactory: async (configService: ConfigService) => ({
        //         uri: configService.get(ENV_VARIABLE_KEY.DB_HOST),
        //     }),
        // }),
    ],
})
export class DatabaseModule {}
