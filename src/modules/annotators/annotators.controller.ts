import { Controller, Post, Get, Param, Body, UseGuards, UseInterceptors, ClassSerializerInterceptor, Request } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Annotator } from './interfaces';
import { GetAnnotatorQuery } from './queries/impl';
import { AuthGuard } from '@nestjs/passport';
import { AnnotatorEntity } from './serializers/annotator.entity';
import { StartSessionDto, FinishSessionDto } from './dto';
import { StartSessionCommand, FinishSessionCommand } from './commands/impl';

@Controller('annotators')
export class AnnotatorsController {
    constructor(
        private readonly commandBus: CommandBus,
        private readonly queryBus: QueryBus,
    ) {}

    // Commands
    @UseGuards(AuthGuard('jwt'))
    @Post(':id/command/startSession')
        async startSession(@Param('id') id: string, @Body() dto: StartSessionDto) {
        return this.commandBus.execute(new StartSessionCommand(id, dto.jobId));
    }

    @UseGuards(AuthGuard('jwt'))
    @Post(':id/command/finishSession')
        async finishSession(@Param('id') id: string, @Body() dto: FinishSessionDto) {
        return this.commandBus.execute(new FinishSessionCommand(id, dto.jobId));
    }

    // Queries
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(ClassSerializerInterceptor)
    @Get(':id/query/getAnnotator')
        async getAnnotator(@Request() req: any): Promise<Annotator> {
        return new AnnotatorEntity(await this.queryBus.execute<GetAnnotatorQuery, Annotator>(new GetAnnotatorQuery(req.user.userId)));
    }
}
