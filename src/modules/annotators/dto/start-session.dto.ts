import { IsString } from 'class-validator';

export class StartSessionDto {
    @IsString()
    public readonly jobId: string;
}
