import { Module } from '@nestjs/common';
import { AnnotatorsModule } from './modules/annotators/annotators.module';
import { CoreModule } from './core/core.module';
import { FramesModule } from './modules/frames/frames.module';
import { AnnotationsModule } from './modules/annotations/annotations.module';
import { JobsModule } from './modules/jobs/jobs.module';
import { ImagesModule } from './modules/images/images.module';
import { SharedModules } from './shared/shared.module';

@Module({
    imports: [
        ...SharedModules,
        CoreModule,
        AnnotatorsModule,
        FramesModule,
        AnnotationsModule,
        JobsModule,
        ImagesModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
