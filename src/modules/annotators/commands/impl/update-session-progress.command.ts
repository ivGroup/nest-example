export class UpdateSessionProgressCommand {
    constructor(
        public readonly annotatorId: string,
        public readonly sessionProgress: number,
        public readonly detectionsQty?: number,
        public readonly detectionsWithTrackId?: number,
    ) {}
}
