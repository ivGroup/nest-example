export class StartSessionCommand {
    constructor(
        public readonly annotatorId: string,
        public readonly jobId: string,
    ) {}
}
