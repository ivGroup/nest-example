import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { ENV_VARIABLE_KEY } from '../constants';

type EnvConfig = { [key in ENV_VARIABLE_KEY]: string };

@Injectable()
export class ConfigService {
    private readonly envConfig: EnvConfig;

    constructor(filePath: string) {
        this.envConfig = dotenv.parse(fs.readFileSync(filePath)) as EnvConfig;
    }

    get(key: ENV_VARIABLE_KEY): string {
        return this.envConfig[key];
    }
}
