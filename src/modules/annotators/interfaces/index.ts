export interface Annotator {
    _id: string;
    email: string;
    password: string;
    first_name: string;
    last_name: string;
    active_sessions: Session[];
}

export interface Session {
    job_id: string;
    session_progress: number;
    detections_qty: number;
    detections_with_track_id: number;
}
