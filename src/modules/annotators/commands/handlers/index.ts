import { UpdateAnnotatorSessionProgressHandler } from './update-annotator-session-progress.handler';

export const CommandHandlers = [UpdateAnnotatorSessionProgressHandler];
