import { Annotator, Session } from '../../interfaces';

export function getAnnotator(): Annotator {
    return {
        _id: '1',
        email: 'test@gmail.com',
        password: 'password',
        first_name: 'Test',
        last_name: 'Individual',
        active_sessions: [] as Session[],
    };
}
