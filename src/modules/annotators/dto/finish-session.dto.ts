import { IsString } from 'class-validator';

export class FinishSessionDto {
    @IsString()
    public readonly jobId: string;
}
